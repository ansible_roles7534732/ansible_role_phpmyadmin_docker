Phpmyadmin_docker role
=========

Installs phpmyadmin docker-compose project

Requirements
------------

—

Role Variables
--------------

See defaults/main.yml

Dependencies
------------

—

Example Playbook
----------------

```
---
- name: Install phpmyadmin
  hosts: web_vms
  become: true
  gather_facts: true
  roles:
    - phpmyadmin_docker
      tags:
        - role_phpmyadmin_docker
```

License
-------

MIT

Author Information
------------------

n98gt56ti@gmail.com
